package entities

import "time"

type Book struct {
	ID        int32 `gorm:"primarykey"`
	Title     string
	Author    string
	PageCount int32
	Language  string
	CreatedAt time.Time
	UpdatedAt time.Time
}
