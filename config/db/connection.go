package db

import (
	"fmt"
	"log"
	"os"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var PGSql *gorm.DB

func PGConn() {
	DB_USER := "root"
	DB_PASS := "root"
	DB_NAME := "book_api"
	DB_HOST := "127.0.0.1"
	DB_PORT := "5432"

	dns := fmt.Sprintf("postgres://%s:%s@%s:%s/%s", DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME)
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second, // Slow SQL threshold
			LogLevel:                  logger.Info, // Log level
			IgnoreRecordNotFoundError: true,        // Ignore ErrRecordNotFound error for logger
			Colorful:                  false,       // Disable color
		},
	)

	db, err := gorm.Open(postgres.New(postgres.Config{
		DSN:                  dns,
		PreferSimpleProtocol: true, // disables implicit prepared statement usage
	}), &gorm.Config{
		Logger: newLogger,
	})
	if err != nil {
		log.Fatalf("Postgre Connection Error: %v", err.Error())
	}
	fmt.Println("PostgreSQL Connected")

	// db.AutoMigrate(&entities.Book{})

	PGSql = db
}
