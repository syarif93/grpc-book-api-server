package book

import (
	"context"
	"errors"
	"fmt"

	"server/config/db"
	"server/config/db/entities"
	"server/proto/pb/book"
)

type Server struct {
	book.UnimplementedBookServiceServer
}

func (s *Server) CreateBook(ctx context.Context, req *book.CreateBookRequest) (*book.CreateBookResponse, error) {
	bookReq := req.GetBook()

	data := &entities.Book{
		Title:     bookReq.Title,
		Author:    bookReq.Author,
		PageCount: bookReq.PageCount,
		Language:  bookReq.Language,
	}
	if err := db.PGSql.Create(&data).Error; err != nil {
		return nil, errors.New("create book error")
	}

	return &book.CreateBookResponse{
		Book: &book.Book{
			Id:        data.ID,
			Title:     data.Title,
			Author:    data.Author,
			PageCount: data.PageCount,
			Language:  data.Language,
			CreatedAt: data.CreatedAt.String(),
			UpdatedAt: data.CreatedAt.String(),
		},
	}, nil
}

func (s *Server) GetBookByID(ctx context.Context, req *book.GetBookByIDRequest) (*book.GetBookByIDResponse, error) {
	bookId := req.BookId

	var bookData entities.Book
	if err := db.PGSql.Where("id = ?", bookId).First(&bookData).Error; err != nil {
		return nil, errors.New("book not found")
	}

	return &book.GetBookByIDResponse{
		Book: &book.Book{
			Id:        bookData.ID,
			Title:     bookData.Title,
			Author:    bookData.Author,
			PageCount: bookData.PageCount,
			Language:  bookData.Language,
			CreatedAt: bookData.CreatedAt.String(),
			UpdatedAt: bookData.CreatedAt.String(),
		},
	}, nil
}

func (s *Server) UpdateBookByID(ctx context.Context, req *book.UpdateBookByIDRequest) (*book.UpdateBookByIDResponse, error) {
	bookId := req.BookId
	bookReq := req.GetBook()

	var bookData entities.Book
	if err := db.PGSql.Where("id = ?", bookId).First(&bookData).Error; err != nil {
		return nil, errors.New("book not found")
	}

	fmt.Println(bookReq)

	if errUpdate := db.PGSql.Model(&bookData).Where("id = ?", bookId).Updates(map[string]any{
		"title":      bookReq.Title,
		"author":     bookReq.Author,
		"page_count": bookReq.PageCount,
		"language":   bookReq.Language,
	}).Error; errUpdate != nil {
		return nil, errors.New("error update book")
	}

	return &book.UpdateBookByIDResponse{
		Book: &book.Book{
			Id:        bookData.ID,
			Title:     bookData.Title,
			Author:    bookData.Author,
			PageCount: bookData.PageCount,
			Language:  bookData.Language,
			CreatedAt: bookData.CreatedAt.String(),
			UpdatedAt: bookData.CreatedAt.String(),
		},
	}, nil
}

func (s *Server) DeleteBookByID(ctx context.Context, req *book.DeleteBookByIDRequest) (*book.DeleteBookByIDResponse, error) {
	bookId := req.BookId

	var bookData entities.Book
	if err := db.PGSql.Where("id = ?", bookId).First(&bookData).Error; err != nil {
		return nil, errors.New("book not found")
	}

	if errDelete := db.PGSql.Where("id = ?", bookId).Delete(&bookData).Error; errDelete != nil {
		return nil, errors.New("error delete book")
	}

	return &book.DeleteBookByIDResponse{
		Book: &book.Book{
			Id:        bookData.ID,
			Title:     bookData.Title,
			Author:    bookData.Author,
			PageCount: bookData.PageCount,
			Language:  bookData.Language,
			CreatedAt: bookData.CreatedAt.String(),
			UpdatedAt: bookData.CreatedAt.String(),
		},
	}, nil
}

func (s *Server) GetBookList(ctx context.Context, in *book.GetBookListRequest) (*book.GetBookListResponse, error) {
	var books []*entities.Book
	if err := db.PGSql.Find(&books).Error; err != nil {
		return nil, errors.New("error get books")
	}

	var booksResp []*book.Book
	for _, bookData := range books {
		booksResp = append(booksResp, &book.Book{
			Id:        bookData.ID,
			Title:     bookData.Title,
			Author:    bookData.Author,
			PageCount: bookData.PageCount,
			Language:  bookData.Language,
			CreatedAt: bookData.CreatedAt.String(),
			UpdatedAt: bookData.UpdatedAt.String(),
		})
	}

	return &book.GetBookListResponse{
		Books: booksResp,
	}, nil
}
