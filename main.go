package main

import (
	"log"
	"net"

	"server/config/db"
	"server/proto/pb/book"
	bookServer "server/repositories/book"

	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func init() {
	db.PGConn()
}

func main() {
	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		panic(err)
	}

	s := grpc.NewServer(
		grpc.UnaryInterceptor(
			grpc_recovery.UnaryServerInterceptor(),
		),
	)
	reflection.Register(s)

	book.RegisterBookServiceServer(s, &bookServer.Server{})

	if err := s.Serve(listener); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
